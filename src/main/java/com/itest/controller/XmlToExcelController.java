package com.itest.controller;

import cn.hutool.core.io.FileUtil;
import com.itest.json.XmlToJson;
import com.itest.service.BaseToExcelService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author: 咏蛙
 * @date: 2022/8/28 20:44
 * @description: testlink xml 转Excel
 */
@Slf4j
@RestController
public class XmlToExcelController {

    @Resource
    BaseToExcelService excelService;

    @Resource
    XmlToJson xmlToJson;

    @SneakyThrows
    @PostMapping("/xmlUpload")
    public String xmlUpload(@RequestParam("file") MultipartFile multipartFile, @RequestParam("filePath")String filePath){
        SAXReader reader = new SAXReader();
        Document document = null;

        try {
            document = reader.read(multipartFile.getInputStream());
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        xmlToJson.documentNode(document.getRootElement().elements("testsuite"));
        // 获取文件名称
        var fileName = filePath + multipartFile.getOriginalFilename() + ".xlsx";

        excelService.save(xmlToJson.getiTestBaseExcelList(),fileName);

        return "文件已解析完成，请查看保存文件的目录！";
    }
}
