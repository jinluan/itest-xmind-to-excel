package com.itest.json;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.itest.pojo.ITestBaseExcel;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Element;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author: 咏蛙
 * @date: 2022/8/28 14:48
 * @description: testlink xml 转JSON
 */
@Slf4j
@Component
public class XmlToJson {
    @Resource
    ITestBaseExcel baseExcel;

    List<ITestBaseExcel> iTestBaseExcelList = new LinkedList<>();

    List<String> testSuiteNameList = new LinkedList<>();

    /**
     * 获取并解析整个文档
     *
     * @param allTestSuiteElements 根节点
     */
    public void documentNode(List<Element> allTestSuiteElements){

        // 获取所有的testsuite 元素
        for (Element testSuite : allTestSuiteElements) {
            log.info("name: {}", testSuite.attributeValue("name"));
            testSuiteNameList.add(testSuite.attributeValue("name"));
            testSuiteNameList.add("/");

            // 设置用例模块
            String projectName = "";
            for (String name : testSuiteNameList) {
                projectName += name;
            }
            baseExcel.setProjectName(projectName);
            // 用例备注
            baseExcel.setRemark(removeAny(testSuite.elementText("details")));

            List<Element> testBaseList = testSuite.elements("testcase");
            // 用例不为空
            if (!testBaseList.isEmpty()) {
                // 获取模块下的所有用例
                for (Element testBase : testBaseList) {

                    // 设置用例名称
                    baseExcel.setBaseDesc(removeAny(testBase.attributeValue("name")));
                    // 设置前置条件
                    baseExcel.setPreCondition(removeAny(testBase.elementText("preconditions")));

                    // 设置用例等级
                    baseExcel.setPriority(getPriority(Integer.valueOf(testBase.elementText("importance"))));

                    // 获取用例步骤
                    var steps = testBase.element("steps");

                    var step = steps.elements("step");
                    for (Element s : step) {
                        // 用例步骤
                        baseExcel.setBaseStep(removeAny(s.elementText("actions")));

                        // 预期结果
                        baseExcel.setBaseResult(removeAny(s.elementText("expectedresults")));
                        // 保存用例信息
                        iTestBaseExcelList.add(BeanUtil.copyProperties(baseExcel, ITestBaseExcel.class));
                    }
                }

            }

            // 获取testsuite
            List<Element> testSuites = testSuite.elements("testsuite");
            if (!testSuites.isEmpty()) {
                documentNode(testSuites);
            }
            testSuiteNameList.remove(testSuiteNameList.size() - 1);
            testSuiteNameList.remove(testSuiteNameList.size() - 1);
        }

    }

    /**
     * 转换用例优先级
     *
     * @param priority  用例等级
     * @return
     */
    private String getPriority(Integer priority){

        switch (priority) {
            case 1 :
                return "低";
            case 2 :
                return "中";
            default:
                return "高";
        }
    }

    /**
     * 去除多余的文件格式
     * @param str
     * @return
     */
    public String removeAny (String str) {
        return StrUtil.removeAny(str, "<p>", "</p>", "<li>", "</li>", "<ol>", "</ol>");
    }

    public List<ITestBaseExcel> getiTestBaseExcelList() {
        return iTestBaseExcelList;
    }
}
