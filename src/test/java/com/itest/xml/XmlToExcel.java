package com.itest.xml;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.XmlUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.itest.pojo.ITestBaseExcel;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author: 咏蛙
 * @date: 2022/8/21 13:22
 * @description: tudo
 */
@Slf4j
public class XmlToExcel {

    ITestBaseExcel baseExcel = new ITestBaseExcel();

    List<ITestBaseExcel> iTestBaseExcelList = new LinkedList<>();

    List<String> testSuiteNameList = new LinkedList<>();

    @Test
    public void test() {

        SAXReader reader = new SAXReader();
        Document document = null;

        try {
            document = reader.read(FileUtil.file("C:\\Users\\78580\\Desktop\\excel\\xml\\demo.testproject-deep.xml"));
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        documentNode(document.getRootElement().elements("testsuite"));

        System.out.println("iTestBaseExcelList = " + iTestBaseExcelList);
    }

    /**
     * 获取整个文档
     *
     * @param allTestSuiteElements 根节点
     */
    private void documentNode (List<Element> allTestSuiteElements){

        // 获取所有的testsuite 元素
        for (Element testSuite : allTestSuiteElements) {
            log.info("name: {}", testSuite.attributeValue("name"));
            testSuiteNameList.add(testSuite.attributeValue("name"));
            testSuiteNameList.add("/");

            // 设置用例模块
            String projectName = "";
            for (String name : testSuiteNameList) {
                projectName += name;
            }
            baseExcel.setProjectName(projectName);
            // 用例备注
            baseExcel.setRemark(removeAny(testSuite.elementText("details")));
            log.info("用例模块: {}, 用例备注信息：{}", baseExcel.getProjectName(), baseExcel.getRemark());

            List<Element> testBaseList = testSuite.elements("testcase");
            // 用例不为空
            if (!testBaseList.isEmpty()) {
                // 获取模块下的所有用例
                for (Element testBase : testBaseList) {
                    log.info("用例名称: {}", testBase.attributeValue("name"));

                    // 设置用例名称
                    baseExcel.setBaseDesc(removeAny(testBase.attributeValue("name")));
                    // 设置前置条件
                    baseExcel.setPreCondition(removeAny(testBase.elementText("preconditions")));
                    log.info("前置条件: {}", testBase.elementText("preconditions"));

                    // 设置用例等级
                    baseExcel.setPriority(getPriority(Integer.valueOf(testBase.elementText("importance"))));

                    // log.info("用例等级: {}", priority);


                    // 获取用例步骤
                    var steps = testBase.element("steps");
                    // log.info("steps: {}", steps.asXML());

                    var step = steps.elements("step");
                    for (Element s : step) {
                        log.info("steps-s: {}", s.asXML());

                        log.info("用例步骤: {}", s.elementText("actions"));
                        // 用例步骤
                        baseExcel.setBaseStep(removeAny(s.elementText("actions")));

                        log.info("用例预期结果: {}", s.elementText("expectedresults"));
                        // 预期结果
                        baseExcel.setBaseResult(removeAny(s.elementText("expectedresults")));
                        // 保存用例信息
                        iTestBaseExcelList.add(BeanUtil.copyProperties(baseExcel, ITestBaseExcel.class));
                    }
                }

            }

            // 获取testsuite
            List<Element> testSuites = testSuite.elements("testsuite");
            if (!testSuites.isEmpty()) {
                documentNode(testSuites);
            }
            testSuiteNameList.remove(testSuiteNameList.size() - 1);
            testSuiteNameList.remove(testSuiteNameList.size() - 1);
        }

    }

    private String getPriority(Integer priority){

        switch (priority) {
            case 1 :
                return "低";
            case 2 :
                return "中";
            default:
                return "高";
        }
    }


    @Test
    public void testFile() throws Exception {
        File file = new File("C:\\Users\\78580\\Desktop\\excel\\xml\\demo.testproject-deep.xml");
        System.out.println(file.toString());
        var str = FileUtil.readUtf8String("C:\\Users\\78580\\Desktop\\excel\\xml\\demo.testproject-deep.xml");
        System.out.println(str);
    }


    public String removeAny (String str) {
        return StrUtil.removeAny(str, "<p>", "</p>", "<li>", "</li>", "<ol>", "</ol>");
    }
}
